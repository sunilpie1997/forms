import { Address } from './address';

/*generated using ng g class user*/
export class User {

    constructor(public name:string,
                public email:string,
                public address:Address,
                public gender:string,
                public preference:string
                ){

    }
}

