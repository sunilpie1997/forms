import { Component } from '@angular/core';
import { User } from './user';
import { Address } from './address';
import { RegisterService } from './register.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles:[`
  
  
  
  `]
  
  
  
})
export class AppComponent {
  title = 'learn-forms';
  //one way binding-->from class to view ,as values changed in template are not reflected in the object.
  //Note:use two way binding
  courses=["angular","react","vue"];
  //binding a model User with a form(for editing,creating new instance ) or vice-versa
  address=new Address("pune","");
  usermodel=new User("sunil","sunilpie1997@gmail.com",this.address,"male","")


  user=new User("","sunilpie1997@gmail.com",this.address,"male","")
  //to check if two way binding works?
  constructor(private _registerService:RegisterService){

  }
  onClick(){
console.log(this.user);
  }
  onSubmit(){
    console.log(this.user);
  this._registerService.enroll(this.user).subscribe(data=>console.log("data"+data),
  error=>console.log("error"+error));
  }
  
}
